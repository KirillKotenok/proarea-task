package com.kirillk.repo;

import com.kirillk.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.swing.*;
import java.util.Optional;

public interface UserRepo extends JpaRepository<User, Long> {
    Optional<User> findUserById(Long userId);

    Optional<User> findUserByEmail(String email);

    void deleteUserByEmail(String email);
}

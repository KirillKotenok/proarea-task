package com.kirillk.service;

import com.kirillk.dto.UserDto;

public interface UserService {

    UserDto findUserById(Long userId);

    UserDto findUserByEmail(UserDto userDto);

    UserDto saveUser(UserDto userDto);

    UserDto updateUser(UserDto userDto);

    void deleteUserById(Long userId);

    void deleteUserByEmail(UserDto userDto);
}

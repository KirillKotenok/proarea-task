package com.kirillk.service;

import com.itextpdf.text.DocumentException;
import com.kirillk.dto.ProductDto;

import java.io.ByteArrayInputStream;
import java.util.List;

public interface GeneratePdfService {
    ByteArrayInputStream generatePdf(Long productId) throws DocumentException;

    ByteArrayInputStream generatePdf(List<ProductDto> productList) throws DocumentException;
}

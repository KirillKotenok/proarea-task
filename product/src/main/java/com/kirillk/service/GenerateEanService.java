package com.kirillk.service;

public interface GenerateEanService {
    String generateEanCode(int hash);
}

package com.kirillk.controller;

import com.itextpdf.text.DocumentException;
import com.kirillk.dto.ProductDto;
import com.kirillk.service.impl.GeneratePdfServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.util.List;

@RestController
@RequestMapping("/cheque")
public class ChequeController {
    GeneratePdfServiceImpl generatePdfService;

    @Autowired
    public ChequeController(GeneratePdfServiceImpl generatePdfService) {
        this.generatePdfService = generatePdfService;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> generateFromOneProduct(@PathVariable Long productId) throws FileNotFoundException, DocumentException {
        var headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=cheque.pdf");
        ByteArrayInputStream bis = generatePdfService.generatePdf(productId);

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));

    }

    @GetMapping(produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> generateFromSeveralProduct(@RequestBody List<ProductDto> productDtoList) throws FileNotFoundException, DocumentException {
        var headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=cheque.pdf");
        ByteArrayInputStream bis = generatePdfService.generatePdf(productDtoList);

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }
}

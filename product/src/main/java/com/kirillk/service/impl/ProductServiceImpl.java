package com.kirillk.service.impl;

import com.kirillk.dto.ProductDto;
import com.kirillk.exception.ResourceNotFoundException;
import com.kirillk.mapper.ProductMapper;
import com.kirillk.repo.ProductRepo;
import com.kirillk.service.GenerateEanService;
import com.kirillk.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class ProductServiceImpl implements ProductService {
    private transient final ProductMapper productMapper;
    private transient final ProductRepo productRepo;
    private transient final GenerateEanService generateEanService;

    @Autowired
    public ProductServiceImpl(ProductMapper productMapper, ProductRepo productRepo, GenerateEanService generateEanService) {
        this.productMapper = productMapper;
        this.productRepo = productRepo;
        this.generateEanService = generateEanService;
    }

    @Transactional
    public ProductDto createProduct(ProductDto productDto) {
        String nameToGenerateEanCode = productDto.getName();
        productDto.setEan(generateEanService.generateEanCode(nameToGenerateEanCode.hashCode()));
        ProductDto savedProductDto = productMapper.toDto(
                productRepo.save(productMapper.toEntity(productDto)));
        return savedProductDto;
    }


    public ProductDto updateProduct(ProductDto productDto) {
        return createProduct(productDto);
    }

    public ProductDto getProductById(Long productId) {
        return productMapper.toDto(productRepo.findById(productId).orElseThrow(ResourceNotFoundException::new));
    }
}

package com.kirillk.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductDto {
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private Double price;
    private String ean;

    public ProductDto(@NotNull String name, @NotNull Double price) {
        this.name = name;
        this.price = price;
    }
}

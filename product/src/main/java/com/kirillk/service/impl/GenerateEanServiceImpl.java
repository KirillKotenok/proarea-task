package com.kirillk.service.impl;

import com.kirillk.service.GenerateEanService;
import org.springframework.stereotype.Service;

@Service
public class GenerateEanServiceImpl implements GenerateEanService {
    @Override
    public String generateEanCode(int hash) {
        if (hash < 0) {
            hash = -1 * hash;
        }
        String stringHash = String.valueOf(hash);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(stringHash);
        int count = 0;
        do {
            if (count == stringHash.length()) {
                count = 0;
            }
            stringBuilder.append(stringHash.charAt(count));
            count++;
            System.out.println(stringBuilder.toString());
        } while (stringBuilder.toString().length() <= 13);
        return stringBuilder.toString();
    }
}


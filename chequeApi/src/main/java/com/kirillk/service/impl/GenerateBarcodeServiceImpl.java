package com.kirillk.service.impl;

import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.BarcodeEAN;
import com.itextpdf.text.pdf.PdfWriter;
import com.kirillk.repo.ProductRepo;
import com.kirillk.service.GenerateBarcodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GenerateBarcodeServiceImpl implements GenerateBarcodeService {
    private transient final ProductRepo productRepo;

    @Autowired
    public GenerateBarcodeServiceImpl(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    @Override
    public Image generateBarcode(String eanCode, PdfWriter p) {
        BarcodeEAN b = new BarcodeEAN();
        b.setCodeType(BarcodeEAN.EAN13);
        b.setCode(eanCode);

        Image codeEANImage = b.createImageWithBarcode(p.getDirectContent(), null, null);
        codeEANImage.setAbsolutePosition(100, 300);
        codeEANImage.scalePercent(100);
        System.out.println("successfully created");
        return codeEANImage;
    }
}

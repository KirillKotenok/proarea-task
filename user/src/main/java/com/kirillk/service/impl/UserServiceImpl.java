package com.kirillk.service.impl;

import com.kirillk.dto.UserDto;
import com.kirillk.entity.User;
import com.kirillk.exception.ResourceNotFoundException;
import com.kirillk.mapper.UserMapper;
import com.kirillk.repo.UserRepo;
import com.kirillk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

public class UserServiceImpl implements UserService {
    private UserMapper userMapper;
    private UserRepo userRepo;

    @Autowired
    public UserServiceImpl(UserMapper userMapper, UserRepo userRepo) {
        this.userMapper = userMapper;
        this.userRepo = userRepo;
    }

    @Override
    public UserDto findUserById(Long userId) {
        return userMapper.toDto(userRepo
                .findUserById(userId)
                .orElseThrow(ResourceNotFoundException::new));
    }

    @Override
    public UserDto findUserByEmail(UserDto userDto) {
        return userMapper.toDto(userRepo
                .findUserByEmail(userDto.getEmail())
                .orElseThrow(ResourceNotFoundException::new));
    }

    @Override
    public UserDto saveUser(UserDto userDto) {
        User savedUser = userRepo.save(userMapper.toEntity(userDto));
        return userMapper.toDto(savedUser);
    }

    @Override
    public UserDto updateUser(UserDto userDto) {
        return saveUser(userDto);
    }

    @Override
    public void deleteUserById(Long userId) {
        userRepo.deleteById(userId);
    }

    @Override
    public void deleteUserByEmail(UserDto userDto) {
        userRepo.deleteUserByEmail(userDto.getEmail());
    }
}

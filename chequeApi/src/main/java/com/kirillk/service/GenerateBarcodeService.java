package com.kirillk.service;

import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

public interface GenerateBarcodeService {
    Image generateBarcode(String eanCode, PdfWriter p);
}

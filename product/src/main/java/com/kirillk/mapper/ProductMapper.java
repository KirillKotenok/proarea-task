package com.kirillk.mapper;

import com.kirillk.dto.ProductDto;
import com.kirillk.entity.Product;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    Product toEntity (ProductDto productDto);
    ProductDto toDto (Product product);
}

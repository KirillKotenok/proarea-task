package com.kirillk.mapper;

import com.kirillk.dto.UserDto;
import com.kirillk.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User toEntity(UserDto userDto);

    UserDto toDto(User user);
}

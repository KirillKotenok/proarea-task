package com.kirillk.service.impl;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.kirillk.dto.ProductDto;
import com.kirillk.service.GeneratePdfService;
import com.kirillk.service.ProductService;
import com.kirillk.service.impl.GenerateBarcodeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

@Service
public class GeneratePdfServiceImpl implements GeneratePdfService {
    private static Font mainFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);
    private transient final GenerateBarcodeServiceImpl generateBarcodeService;
    private transient final ProductService productService;

    @Autowired
    public GeneratePdfServiceImpl(GenerateBarcodeServiceImpl generateBarcodeService, ProductService productService) {
        this.generateBarcodeService = generateBarcodeService;
        this.productService = productService;
    }


    @Override
    public ByteArrayInputStream generatePdf(Long productId) throws DocumentException {
        ProductDto productDto = productService.getProductById(productId);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Document document = new Document(new Rectangle(PageSize.A6));
        PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
        document.open();
        generateMainText(document, productDto);
        generateSum(document, productDto.getPrice());
        document.add(generateBarcodeService.generateBarcode(productDto.getEan(), pdfWriter));
        document.close();
        return new ByteArrayInputStream(out.toByteArray());
    }

    @Override
    public ByteArrayInputStream generatePdf(List<ProductDto> productList) throws DocumentException {
        Double sum = 0.;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Document document = new Document(new Rectangle(PageSize.A6));
        PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
        document.open();
        for (ProductDto product : productList) {
            generateMainText(document, product);
            document.add(generateBarcodeService.generateBarcode(product.getEan(), pdfWriter));
            sum += product.getPrice();
        }
        generateSum(document, sum);
        document.close();
        return new ByteArrayInputStream(out.toByteArray());
    }


    private void generateMainText(Document d, ProductDto productDto) throws DocumentException {
        //Prepare product info
        Paragraph productNameTitle = new Paragraph("Product name", mainFont);
        productNameTitle.setAlignment(Element.ALIGN_CENTER);
        productNameTitle.setSpacingAfter(15);
        d.add(productNameTitle);

        Paragraph productName1 = new Paragraph(productDto.getName(), subFont);
        productName1.setAlignment(Element.ALIGN_CENTER);
        productName1.setSpacingAfter(20);
        d.add(productName1);

        Paragraph priceTitle = new Paragraph("Price", mainFont);
        priceTitle.setAlignment(Element.ALIGN_CENTER);
        priceTitle.setSpacingAfter(15);
        d.add(priceTitle);

        Paragraph price = new Paragraph(String.valueOf(productDto.getPrice()), subFont);
        price.setAlignment(Element.ALIGN_CENTER);
        price.setSpacingAfter(20);
        d.add(price);
    }

    private void generateSum(Document d, Double sum) throws DocumentException {
        Paragraph sumTitle = new Paragraph("All price", mainFont);
        sumTitle.setAlignment(Element.ALIGN_CENTER);
        sumTitle.setSpacingAfter(15);
        d.add(sumTitle);

        Paragraph sumText = new Paragraph(String.valueOf(sum), subFont);
        sumText.setAlignment(Element.ALIGN_CENTER);
        sumText.setSpacingAfter(20);
        d.add(sumText);
    }
}

package com.kirillk.controller;

import com.kirillk.dto.ProductDto;
import com.kirillk.service.impl.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product")
public class ProductController {
    private ProductServiceImpl productServiceImpl;

    @Autowired
    public ProductController(ProductServiceImpl productServiceImpl) {
        this.productServiceImpl = productServiceImpl;
    }

    @PostMapping
    public ResponseEntity<ProductDto> saveProduct(@Validated @RequestBody ProductDto productDto) {
        ProductDto savedProduct = productServiceImpl.createProduct(productDto);
        return new ResponseEntity<>(savedProduct, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<ProductDto> updateProduct(@Validated @RequestBody ProductDto productDto) {
        ProductDto updatedProduct = productServiceImpl.updateProduct(productDto);
        return new ResponseEntity<>(updatedProduct, HttpStatus.CREATED);
    }
}
